import {configure, addParameters} from '@storybook/angular';
import {themes} from '@storybook/theming';

// addParameters({
//     options: {
//         theme: themes.dark,
//     },
// });

function loadStories() {
    require('../stories/index');
    // You can require as many stories as you need.
}

configure(loadStories, module);

