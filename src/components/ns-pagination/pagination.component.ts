import {Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'ns-pagination',
    templateUrl: './pagination.template.html',
    styleUrls: ['./pagination.scss']
})
export class PaginationComponent implements OnInit, OnChanges {

    // 分頁下拉選單選項
    @Input('nsPageSizeOption') pageSizeOption: { id: number; size: number }[];

    // 總筆數
    @Input('nsTotalCount') totalCount: number;

    // 是否顯示頁數總數量和從輸入框輸入數字改頁數
    @Input('nsShowInput') showInput: boolean = true;

    // 是否顯示固定的每頁比數
    @Input('nsFixedPageSize') fixedPageSize: boolean = false;

    // 是否不要顯示每頁筆數
    @Input('nsShowNoCounts') showNoCounts: boolean;

    // 當前一頁幾筆筆數
    @Input('nsPageSize') pageSize: number;
    @Output('nsPageSizeChange') pageSizeChange = new EventEmitter();

    // 目前頁數
    @Input('nsPage') page: number;
    @Output('nsPageChange') pageChange = new EventEmitter();

    //停留在哪一筆資料
    @Input('nsPageIndex') pageIndex: number = 0;
    @Output('nsPageIndexChange') pageIndexChange = new EventEmitter();

    // 當前下拉選單選項的Id
    selectedPageSize: number;

    // 頁數總數量
    totalPageCount: number;

    // input 輸入頁數
    pageInput: number;

    //分頁下拉選單預設值
    defaultPageSizeOption = [{id: 0, size: 25}, {id: 1, size: 50}, {id: 2, size: 100}, {id: 3, size: 150}];

    constructor() {
    }

    ngOnInit() {
        // 處理一頁幾筆下拉選單
        this.pageSizeOption = this.pageSizeOption || this.defaultPageSizeOption;
        this.pageSize = this.pageSize || this.pageSizeOption[0].size;
        this.selectedPageSize = this.pageSizeOption.find(item => item.size === this.pageSize).id;

        // 算總頁數
        this.totalPageCount = Math.ceil(this.totalCount / this.pageSize);

        // 計算當前頁碼
        //this.page = Math.floor(this.pageIndex / this.pageSize) + 1;
        this.page = 0;
        this.pageInput = this.page;

        // 通知外部改變
        this.pageSizeChange.emit(this.pageSize);
        this.pageChange.emit(this.page);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.totalCount && !changes.totalCount.isFirstChange()) {
            this.ngOnInit();
        }

        if (changes.pageSize && !changes.pageSize.isFirstChange()) {
            this.changePageSize();
        }
    }

    // 改變 一頁幾筆設定(下拉選單or外部)
    changePageSize() {
        this.pageSize = this.pageSizeOption[this.selectedPageSize].size;
        this.totalPageCount = Math.ceil(this.totalCount / this.pageSize);
        this.page = 1;
        this.pageInput = 1;
        this.pageIndex = 0;
        this.pageSizeChange.emit(this.pageSize);
        this.pageChange.emit(this.page);
        this.pageIndexChange.emit(this.pageIndex);
    }

    // 透過 ngb-pagination 元件切換頁碼
    changePage() {
        if (this.page >= 0 || this.page <= this.totalPageCount) {
            this.pageInput = this.page;
            this.pageIndex = (this.page - 1) * this.pageSize;
            this.pageChange.emit(this.page);
            this.pageIndexChange.emit(this.pageIndex);
        }
    }

    // input 切換頁碼
    changePageByInput($event) {
        if ($event.which === 13) {
            let page = parseInt($event.currentTarget.value);
            if (page <= 0 || page > this.totalPageCount) {
                return;
            }
            this.page = page;
            this.changePage();
        }
    }
}