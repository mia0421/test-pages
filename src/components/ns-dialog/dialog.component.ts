import {Component, Input, ViewEncapsulation} from '@angular/core';

import {NgbModal, NgbModalConfig, NgbActiveModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'dialog-confirm-content',
    templateUrl: './dialog.confirm.template.html',
    styleUrls: ['./dialog.scss'],
})
export class DialogConfirmContent {
    // 內文文案
    @Input() message: string;

    // header 文案
    @Input() title: string;

    // header 上的關閉岸鳥
    @Input() showCancel: boolean;

    // 關閉的button
    @Input() showCancelButton: boolean;

    // 關閉button的文案
    @Input() cancelMessage: string;

    // 確定button的文案
    @Input() acceptMessage: string;

    constructor(public activeModal: NgbActiveModal) {
    }
}


@Component({
    selector: 'ns-dialog',
    template: `
        <div></div>`,
    encapsulation: ViewEncapsulation.None
})
export class DialogComponent {
    constructor(
        private modalService: NgbModal,
        private config: NgbModalConfig) {
        config.backdrop = 'static';
    }

    /**
     * 通知視窗
     * @param message 顯示訊息
     * @param title 標題
     * @param showCloseButton 是否顯示右上角關閉按鈕
     */
    notice(message: string, title?: string, showCloseButton?: boolean) {
        let options: NgbModalOptions = {
            centered: true,
            size: 'sm',
        };
        let modalRef = this.modalService.open(DialogConfirmContent, options);
        modalRef.componentInstance.message = message;
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.showCloseButton = showCloseButton;
        modalRef.componentInstance.showCancel = false;
        return modalRef.result;
    }

    /**
     * 確認視窗
     * @param message 顯示訊息
     * @param title 標題
     * @param showCancel 是否顯示取消按鈕
     * @param showCloseButton 是否顯示右上角關閉按鈕
     * @param acceptMessage 接受按鈕文字
     * @param cancelMessage 取消按鈕文字
     */
    confirm(message: string, title?: string, showCancel?: boolean, showCloseButton?: boolean, acceptMessage?: string, cancelMessage?: string) {
        let options: NgbModalOptions = {
            centered: true,
            size: 'sm',
        };
        const modalRef = this.modalService.open(DialogConfirmContent, options);
        modalRef.componentInstance.message = message;
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.showCancel = showCancel;
        modalRef.componentInstance.showCloseButton = showCloseButton;
        modalRef.componentInstance.acceptMessage = acceptMessage;
        modalRef.componentInstance.cancelMessage = cancelMessage;
        return modalRef.result;

    }

}