import { Component } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
@Component({
    selector: 'ns-loading',
    templateUrl: './loading.template.html',
    styleUrls: ['./loading.scss']
})

export class LoadingComponent {
    @BlockUI() blockUI: NgBlockUI;

    constructor() {
    }
    /**
     * 啟動Block UI
     * @param message
     */
    start(message?: string) {
        this.blockUI.start(message);
    }
    /**
     * 關閉Block UI
     */
    stop() {
        this.blockUI.stop();
    }
}
