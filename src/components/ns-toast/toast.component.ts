import { Component, ViewEncapsulation, TemplateRef  } from '@angular/core';
import { NotificationService } from '@progress/kendo-angular-notification';
import { Observable } from 'rxjs/Observable';
/**
 * 因使用KendoUI自身template，所以不需要定義template component
 */
@Component({
    selector: 'ns-toast',
    styleUrls: ['./toast.scss'],
    template:"",
    encapsulation: ViewEncapsulation.None
})

export class ToastComponent {
    // 顯示Toast時間
    public duration: number = 800;
    // 隱藏Toast時間
    public hideAfter: number = 800;

    constructor(
        private notificationService: NotificationService
    ) {
    }
    /**
     * 顯示通知訊息
     * @param message 
     */
    show(message: string) {
        return new Observable((observer) => {
            this.notificationService.show({
                content: message,
                // 設定垂直置中Css
                cssClass: 'notification-container',
                animation: { type: 'fade', duration: this.duration },
                position: { horizontal: 'center', vertical: 'top' },
                type: { style: 'none', icon: false },
                hideAfter: this.hideAfter
            });
            window.setTimeout(() => {
                observer.next();
            }, 2000);
        });
    }
    /**
     * 自定義內容樣板
     * @param template 
     * @param styleClass 
     */
    custom(template: TemplateRef<any>, styleClass?: string) {
        this.notificationService.show({
            content: template,
            cssClass: styleClass,
            animation: { type: 'fade', duration: this.duration },
            position: { horizontal: 'center', vertical: 'top' },
            type: { style: 'none', icon: false },
            hideAfter: this.hideAfter
        });
    }
}
