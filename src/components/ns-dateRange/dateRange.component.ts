import { Component, Output, Input } from '@angular/core';

@Component({
    selector: 'ns-date-range',
    templateUrl: './dateRange.template.html'
})

export class DateRangeComponent {

    @Input('ngStartDate') startDate: Date;
    @Input('ngEndDate') endDate: Date;


}