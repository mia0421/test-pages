import {CommonModule} from '@angular/common';
import {ModuleWithProviders, NgModule} from '@angular/core';

import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {NotificationModule} from '@progress/kendo-angular-notification';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
// custom module
import {PaginationComponent} from './ns-pagination/pagination.component';
import {DateRangeComponent} from './ns-dateRange/dateRange.component';
import {ToastComponent} from './ns-toast/toast.component';
import {DialogComponent, DialogConfirmContent} from "./ns-dialog/dialog.component";
import {LoadingComponent} from './ns-loading/loading.component';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        FormsModule,
        DropDownsModule,
        DateInputsModule,
        NotificationModule,
        AngularFontAwesomeModule,
        BrowserModule,
        BrowserAnimationsModule
    ],
    // view需要使用的template component
    declarations: [
        PaginationComponent,
        DateRangeComponent,
        DialogComponent,
        DialogConfirmContent,
        ToastComponent,
        LoadingComponent
    ],
    exports: [
        PaginationComponent,
        DateRangeComponent,
        DialogComponent,
        ToastComponent,
        LoadingComponent
    ]
})

export class DaliModule {
    // 建立單一實例
    static forRoot(providers = []): ModuleWithProviders {
        return {
            ngModule: DaliModule,
            // 提供外部呼叫方法
            providers: [
                ...providers,
                ToastComponent,
                DialogComponent,
                LoadingComponent
            ]
        };
    }
}

