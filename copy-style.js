
// 複製style 到dist下
const path = require('path');
const fs = require('fs-extra');

const sourcePath = path.resolve(__dirname, './src');
const targetPath = path.resolve(__dirname, './dist');


fs.copySync(path.resolve(sourcePath, 'style'), path.resolve(targetPath, 'style'));
console.log('---------------copy style folder done-----------------------');