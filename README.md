### 環境安裝

---

安裝相依的node_module套件

```
npm i
```

安裝Mkdocs 相依套件

```$xslt
pip3 install mkdocs
```

安裝 Mkdocs theme material 
```$xslt
pip3 install mkdocs-material
```

### 啟動環境

---
啟動StroryBook
```$xslt
npm run storybook
```
``啟動MKdocs``
此步驟將會建立靜態資源與啟動locale站台
```$xslt
npm run mkdocs
```

### 部署Liberal

---

```$xslt
npm run build:lib

cd dist

npm set registry http://npm.91app.io
npm adduser --registry http://npm.91app.io

npm publish
```





