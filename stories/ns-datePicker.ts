import {storiesOf, moduleMetadata} from '@storybook/angular';
import {defaultModule, kendoModule, componentsModule} from './modules';

storiesOf('DatePicker', module)
    .addDecorator(
        moduleMetadata({
            imports: [
                ...defaultModule,
                ...kendoModule,
                ...componentsModule
            ]
        })
    )
    .add('default', () => {
        return {
            template: `<div style="padding:20px">
            <h5>Default</h5>
            <kendo-datepicker [value]="value" [format]="'yyyy/MM/dd'" placeholder="Enter..."></kendo-datepicker>
            <kendo-timepicker [(value)]="value"></kendo-timepicker><br/><br/>
            <h5>Disabled</h5>
            <kendo-datepicker [value]="value" [format]="'yyyy/MM/dd'" [disabled]="isDisabled" placeholder="Enter...">
            </kendo-datepicker><kendo-timepicker [(value)]="value" [disabled]="isDisabled"></kendo-timepicker><br/><br/>
            <h5>Range: </h5>
            <div>Min : {{min | date:'medium'}}</div>
            <div>Max : {{max | date:'medium'}}</div>
            <kendo-datepicker [value]="value" [format]="'yyyy/MM/dd'" [min]="min" [max]="max" placeholder="Enter..."></kendo-datepicker>
            <kendo-timepicker [(value)]="value" [min]="min" [max]="max"></kendo-timepicker>
            </div>
            `,
            props: {
                value: new Date(),
                isDisabled: true,
                min: new Date(2019, 5, 1, 2, 30),
                max: new Date(2019, 5, 30, 22, 30)
            },
        };
    }).add('Date Range', () => {
    return {
        template: `
            <div style="padding: 20px">
                <div class="example-config">
                   日期區間: {{ trange.startDate | date: 'yyyy/MM/dd' }} ~ {{ trange.endDate | date:'yyyy/MM/dd'}}
                </div>
                <ns-date-range #trange [ngStartDate]="startDate" [ngEndDate]="endDate"></ns-date-range>
           </div>
        `,
    }
});

