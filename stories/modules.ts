// 共用的 ,可以整理起來
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms'

import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {DatePickerModule, TimePickerModule, DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {DaliModule} from '../src/components/components.module';

export const componentsModule = [DaliModule];
export const defaultModule = [BrowserModule, BrowserAnimationsModule, FormsModule, NgbModule];
export const kendoModule = [DropDownsModule, DatePickerModule, TimePickerModule, DateInputsModule];