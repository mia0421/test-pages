import {storiesOf, moduleMetadata} from '@storybook/angular';
import {withNotes} from '@storybook/addon-notes';
import {action} from '@storybook/addon-actions';
import {
    withKnobs,
    number
} from '@storybook/addon-knobs';


import {defaultModule, kendoModule, componentsModule} from './modules';

storiesOf('ns-pagination', module)
    .addDecorator(withKnobs)
    .add('default', () => {
            const totalCount = number('總筆數', 888);
            const pageIndex = number('目前頁碼Start Index', 0);
            const pageSize = number('當前一頁幾筆筆數', 25);
            const page = number('目前頁碼', 0);

            return {
                moduleMetadata: {
                    imports: [
                        ...defaultModule,
                        ...kendoModule,
                        ...componentsModule
                    ]
                },
                template: `<div style="padding: 3rem">
                            <ns-pagination [nsTotalCount]="totalCount" [(nsPageIndex)]="pageIndex" [(nsPageSize)]="pageSize" [(nsPage)]="page"> </ns-pagination>
                        </div>`,
                props: {
                    totalCount,
                    pageIndex,
                    pageSize,
                    page
                }
            };
        },
        {notes: '這是一個分頁元件'});
