// angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/compiler';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/forms';
import '@angular/router';

import '@progress/kendo-theme-bootstrap'

// RxJS
import 'rxjs';

// Bootstrap
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

// Component
import './ns-pagination';
import './ns-datePicker';
import './ns-dialog';

import "../node_modules/font-awesome/css/font-awesome.css";
