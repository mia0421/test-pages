import {Component, Input, OnInit} from '@angular/core';

import {storiesOf, moduleMetadata} from '@storybook/angular';

import {DialogComponent, DialogConfirmContent} from "../src/components/ns-dialog/dialog.component";
import {defaultModule, kendoModule, componentsModule} from './modules';

@Component({
    selector: 'dialog-story',
    providers: [DialogComponent],
    template: `        
        <div style="padding: 20px">
            <button class="btn btn-outline-primary btn-sm" *ngIf="testType==='Notice'" (click)="openNoticeDialog()">
                Open Notice Dialog
            </button>
            <button class="btn btn-outline-primary btn-sm" *ngIf="testType==='Confirm'"
                    (click)="openConfirmDialog()">Open Confirm Dialog
            </button>
        </div>
        
    `,
})
export class DialogStory implements OnInit {
    @Input() testType: string;
    @Input() message: string;
    @Input() title: string;
    @Input() showCancel: boolean;
    @Input() showCloseButton: boolean;
    @Input() acceptMessage: string;
    @Input() cancelMessage: string;

    constructor(private dialogComponent: DialogComponent) {

    }


    openNoticeDialog() {
        this.dialogComponent.notice(this.message, this.title, this.showCloseButton)
    }

    openConfirmDialog() {
        this.dialogComponent.confirm(this.message, this.title, this.showCancel, this.showCloseButton, this.acceptMessage, this.cancelMessage);
    }
}

storiesOf('ns-dialog', module)
    .addDecorator(
        moduleMetadata({
            imports: [
                ...defaultModule,
                ...kendoModule,
                ...componentsModule
            ],
            entryComponents: [
                DialogConfirmContent
            ],
            declarations: [
                DialogStory,
            ]
        })
    )
    .add('Default Notice', () => ({
        template: `
        <dialog-story [testType]="testType"
                      [message]="dialogMessage"
                      [title]="dialogTitle"
                      [showCancel]="dialogShowCancel"
                      [showCloseButton]="dialogShowCloseButton"
                      [acceptMessage]="dialogAcceptMessage"
                      [cancelMessage]="dialogCancelMessage">
        </dialog-story>
        `,
        props: {
            testType: 'Notice',
            dialogMessage: 'Demo message!',
            dialogTitle: '標題',
            dialogShowCancel: false,
            dialogShowCloseButton: true,
            dialogAcceptMessage: '',
            dialogCancelMessage: '',
        }
    }))
    .add('Confirm', () => ({
        template: `
        <dialog-story [testType]="testType"
                      [message]="dialogMessage"
                      [title]="dialogTitle"
                      [showCancel]="dialogShowCancel"
                      [showCloseButton]="dialogShowCloseButton"
                      [acceptMessage]="dialogAcceptMessage"
                      [cancelMessage]="dialogCancelMessage">
        </dialog-story>
        `,
        props: {
            testType: 'Confirm',
            dialogMessage: 'Confirm message!',
            dialogTitle: 'Confirm 標題',
            dialogShowCancel: true,
            dialogShowCloseButton: true,
            dialogAcceptMessage: 'AcceptBtn',
            dialogCancelMessage: 'CancelBtn',
        }
    }));